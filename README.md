# ASI2-FrontEnd-Readjs-tp
This is a simple tutorial for the creation of an User form and a set of User visualization
Current version use Hooks

## Step 1 : Creation of a simple User display component
* see [step1 readme](../step1/)


## Step 2 : Creation of an user form
* see [step2 readme](../step2/)

## Step 3 : Creation of multiple user display
* see [step3 readme](../step3/)

## Step 4 : Usage of redux to spread current user updates
* see [step4 readme](../step4/)

## Step 5 : Usage of react router v6
* see [step5 readme](../step5/)
