import React, { useState } from 'react';
import { Grid, Segment } from 'semantic-ui-react'
import {User} from './components/user/containers/User'

//Create function component
export const App =(props) =>{
    const [currentUser,setCurrentUser]= useState({
                                        id:12,
                                        surname:"John",
                                        lastname:"Doe",
                                        login:"jDoe",
                                        pwd:"jdoepwd",
                                        money:1000,
                                      });
     function callbackErr(data){
        console.log(data);
    };

    return (
      <Grid divided='vertically'>
        <Grid.Row columns={2}>
        <Grid.Column>

          <Segment>
            <h1>Some Other Content</h1>

          </Segment>

        </Grid.Column>
        <Grid.Column>
            <User 
                   id={currentUser.id}
                    surname={currentUser.surname}
                    lastname={currentUser.lastname}
                    login={currentUser.login}
                    pwd={currentUser.pwd}
                    money={currentUser.money}> 
            </User>
        </Grid.Column>
        </Grid.Row>
      </Grid>
     
    );
}
