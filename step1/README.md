# Simple User display component

<img src="./img/display.png" alt="display step 3" width="600"/>

## Create reactJS env.
First you need to get all ressources needed to start a react project
1. Create a reactJs app my-app
   ```
    npm create vite@latest my-app -- --template react 

   ```
2. Install reatJs npm dependencies and
   ```
    cd my-app
    npm install 
   ```

3. start your app
    ```
    npm run dev
    ```

    your first react app is generated and started

## Create a App.jsx component
Our final objective is to create a User Form with different User Display
We first need to create a main container that contains everything.
We will use the Semantic UI library. This library has officially been integrated to React (as set of react components) (ref)[https://react.semantic-ui.com/]

Install the Semantic ui library (see installation reference (here)[https://react.semantic-ui.com/usage)]
```
npm install semantic-ui-react semantic-ui-css
```

Modify the main.jsx by added the following reference to Semantic UI:

```javascript
...
//add Semantic UI import
import 'semantic-ui-css/semantic.min.css'
...
```

Modify the App.jsx in the src folder as follow:
```javascript
import React, { useState } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import User from './components/user/containers/User';

//Create function component
export const App =(props) =>{
    const [currentUser,setCurrentUser]= useState({
                                        id:12,
                                        surname:"John",
                                        lastname:"Doe",
                                        login:"jDoe",
                                        pwd:"jdoepwd",
                                        money:1000,
                                      });
     function callbackErr(data){
        console.log(data);
    };

    return (
      <Grid divided='vertically'>
        <Grid.Row columns={2}>
        <Grid.Column>

          <Segment></Segment>

        </Grid.Column>
        <Grid.Column>

        <Segment>
           <h1> Here user display </h1>
        </Segment>
  
        </Grid.Column>
        </Grid.Row>
      </Grid>
     
    );
}
```

- Explanation
  - `import React, { useState } from 'react';` import the component for React especially `useState` use for hooks.
  - `import { Grid, Segment } from 'semantic-ui-react';` import the Semantic ui components.
  - `export const App =(props) =>{ ... }` hooks declaration.
  - `const [currentUser,setCurrentUser]= useState({ id:12... });` state declaration into a hooks. The values is used like this during the render `{currentuser}`. To Modify the state, the following code should be used `setCurrentUser({id:55...});` .

Start your app
```
npm start
```

## Create a UserSimpleDisplay.jsx component
Now we will add components to our app like User or UserForm component.
First create into the my-app/src folder components folder such as:
```
my-app/src/components/user/components
my-app/src/components/user/containers
```

* **user/components** will contain all "visual" components of User
* **user/containers** will contain all "processing" components

In **user/components** create the file **UserSimpleDisplay.jsx** such as:
```javascript
    import React from 'react';
import { Card, Image, Icon } from 'semantic-ui-react';

 export const UserSimpleDisplay=(props) =>{
    return (
        <Card>
            <Image src='https://www.nicepng.com/png/full/982-9820051_heart-2352306885-deadpool-png.png' wrapped ui={false} />
            <Card.Content>
                <Card.Header>{props.surname} {props.lastname} </Card.Header>
                <Card.Meta>
                    <span className='date'>login: {props.login}</span>
                </Card.Meta>
                <Card.Description>
                    User In DataBase
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                    <a>
                        <Icon name='money bill alternate outline' />
                            {props.money} $
                    </a>
            </Card.Content>
        </Card>

        );
    }
```

This component only display information transmitted by its parent.

In order to test it add the following line to the **App.jsx** file:

```javascript
...
import {UserSimpleDisplay} from './components/user/components/UserSimpleDisplay';
...

```

Replace the following line:
```html
<h1> Here user display </h1>
```
by
```html
    <UserSimpleDisplay 
                    id={currentUser.id}
                    surname={currentUser.surname}
                    lastname={currentUser.lastname}
                    login={currentUser.login}
                    pwd={currentUser.pwd}
                    money={currentUser.money}>
    </UserSimpleDisplay>

```

## Create a User component associated to the UserSimpleDisplay

The user entity will need to have different display and a unique point of control.
We will add an intermediate component User to manage user values adn displays.

Create in the folder **components/user/containers** the file **User.jsx**:

```javascript
import React from 'react';
import {UserSimpleDisplay} from '../components/UserSimpleDisplay';

 export const User=(props)=> {
        return ( 
                <UserSimpleDisplay 
                    id={props.id}
                    surname={props.surname}
                    lastname={props.lastname}
                    login={props.login}
                    pwd={props.pwd}
                    money={props.money}> 
                </UserSimpleDisplay>
            );
    }
```

Modify the App.jsx as follow:

Replace the line :
```javascript
import {UserSimpleDisplay} from './components/user/components/UserSimpleDisplay';
```

by

```javascript
import {User} from './components/user/containers/User';
```
 
 Replace the line :
```html
    <UserSimpleDisplay 
                    id={currentUser.id}
                    surname={currentUser.surname}
                    lastname={currentUser.lastname}
                    login={currentUser.login}
                    pwd={currentUser.pwd}
                    money={currentUser.money}>
    </UserSimpleDisplay>

```

by

```html
    <User 
                    id={currentUser.id}
                    surname={currentUser.surname}
                    lastname={currentUser.lastname}
                    login={currentUser.login}
                    pwd={currentUser.pwd}
                    money={currentUser.money}>
    </User>

```

