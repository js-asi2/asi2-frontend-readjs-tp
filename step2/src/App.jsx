import React, { useState } from 'react';
import { Grid, Segment } from 'semantic-ui-react'
import {User} from './components/user/containers/User'
import {UserForm} from './components/userform/UserForm'

//Create function component
export const App =(props) =>{
    const [currentUser,setCurrentUser]= useState({
                                        id:12,
                                        surname:"John",
                                        lastname:"Doe",
                                        login:"jDoe",
                                        pwd:"jdoepwd",
                                        img:'https://www.nicepng.com/png/full/982-9820051_heart-2352306885-deadpool-png.png',
                                        money:1000,
                                      });
     function callbackErr(data){
        console.log(data);
    };

    function handleChange(data){
      console.log(data);
      setCurrentUser({
        id:data.id,
        surname:data.surname,
        lastname:data.lastname,
        login:data.login,
        pwd:data.pwd,
        money:data.money,
        img:data.img,
      });
    };

    function submitUserHandler(data){
      console.log("user to submit"+data);
    };


    return (
      <Grid divided='vertically'>
        <Grid.Row columns={2}>
        <Grid.Column>

          <Segment>
          <UserForm 
                    handleChange={handleChange}
                    submitUserHandler={submitUserHandler}>
                </UserForm>
          </Segment>
        </Grid.Column>
        <Grid.Column>
            <User 
                   id={currentUser.id}
                    surname={currentUser.surname}
                    lastname={currentUser.lastname}
                    login={currentUser.login}
                    pwd={currentUser.pwd}
                    money={currentUser.money}
                    img={currentUser.img}>
            </User>
        </Grid.Column>
        </Grid.Row>
      </Grid>
    );
}
