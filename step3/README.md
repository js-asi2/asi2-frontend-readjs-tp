# Create Multiple User display

We want to have different display of an user. 


<img src="./img/display.png" alt="display step 3" width="600"/>


Create the file **UserShortDisplay.jsx** in the folder **components/user/components**
```javascript
import React from 'react';
import { Feed, Icon } from 'semantic-ui-react'

 export const UserShortDisplay=(props)=>{
   return (
            <Feed>
                <Feed.Event>
                    <Feed.Label>
                        <img src={props.img} />
                    </Feed.Label>
                <Feed.Content>
                <Feed.Summary>
                <a>{props.surname} {props.lastname}</a>
                </Feed.Summary>
                <Feed.Meta>
                    <Feed.Like>
                    <Icon name='money bill alternate outline' />{props.money}
                    </Feed.Like>
                </Feed.Meta>
                    
                </Feed.Content>
                
                </Feed.Event>
            </Feed>
            );
    }
```

Update the User.jsx file as follow:

```javascript
import React from 'react';
import {UserSimpleDisplay} from '../components/UserSimpleDisplay';
import {UserShortDisplay} from '../components/UserShortDisplay'

const FULL_LABEL='FULL';
const SHORT_LABEL='SHORT';

export const User=(props)=> {
    let display="";
    switch(props.display_type){
        case SHORT_LABEL:
            display = (                
                <UserShortDisplay 
                    surname = {props.surname}
                    lastname = {props.lastname}
                    img = {props.img}
                    money = {props.money}> 
                </UserShortDisplay>
            );

            break;
        case FULL_LABEL:
            display=(                
                <UserSimpleDisplay 
                    id = {props.id}
                    surname = {props.surname}
                    lastname = {props.lastname}
                    login = {props.login}
                    pwd = {props.pwd}
                    money = {props.money}
                    img = {props.img}> 
                </UserSimpleDisplay>
            );
            break;
        default:
            display=(<h4>No Display Available</h4>);
    }
        return display;
    }
```

**User.jsx Modification explanation**
We add a props value ```props.display_type ``` that allows the parent to select the display mode of the user.

When the component needs to be repainted it checks the display_type value to know which display is needed ``` switch(props.display_type) ```

Update the App.jsx like that:

```javascript
import React, { useState } from 'react';
import { Grid, Segment,Menu } from 'semantic-ui-react'
import {User} from './components/user/containers/User'
import {UserForm} from './components/userform/UserForm'

//Create function component
export const App =(props) =>{
    const [currentUser,setCurrentUser]= useState({
                                        id:12,
                                        surname:"John",
                                        lastname:"Doe",
                                        login:"jDoe",
                                        pwd:"jdoepwd",
                                        img:'https://www.nicepng.com/png/full/982-9820051_heart-2352306885-deadpool-png.png',
                                        money:1000,
                                      });
     function callbackErr(data){
        console.log(data);
    };

    function handleChange(data){
      console.log(data);
      setCurrentUser({
        id:data.id,
        surname:data.surname,
        lastname:data.lastname,
        login:data.login,
        pwd:data.pwd,
        money:data.money,
        img:data.img,
      });
    };

    function submitUserHandler(data){
      console.log("user to submit"+data);
    };

    return (
      <>
        <Menu>
          <Menu.Item
            name='heropres'
          >
            Hero Presentation
          </Menu.Item>
        </Menu>
        <Grid divided='vertically'>
          <Grid.Row columns={3}>
          <Grid.Column>
            <Segment>
            <UserForm 
                      handleChange={handleChange}
                      submitUserHandler={submitUserHandler}>
                  </UserForm>
            </Segment>
          </Grid.Column>
          <Grid.Column>
              <User 
                     id={currentUser.id}
                      surname={currentUser.surname}
                      lastname={currentUser.lastname}
                      login={currentUser.login}
                      pwd={currentUser.pwd}
                      money={currentUser.money}
                      img={currentUser.img}
                      display_type='FULL'>
              </User>
          </Grid.Column>
          <Grid.Column>
            <User id={currentUser.id}
                      surname={currentUser.surname}
                      lastname={currentUser.lastname}
                      login={currentUser.login}
                      pwd={currentUser.pwd}
                      money={currentUser.money}
                      img={currentUser.img}
                      display_type='SHORT'>
            </User>
          </Grid.Column>
          </Grid.Row>
        </Grid>
      </>
    );
}

```


