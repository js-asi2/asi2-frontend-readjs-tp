# Use Redux to spread current update modifications

<img src="./img/display.png" alt="display step 4" width="600"/>

# Tools needed
- Add the following packages to use redux into react:

```
    npm install redux
    npm install react-redux
    npm install @reduxjs/toolkit
```

# Slice Creation
To exchange information through redux, we need to define a slice that contain both actions an reducer
1. Create the **slices** folder inside the **src** folder.
2. Create a file **userSlice.js** in **src/slices** as follow:

```javascript
import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
  name: 'User',
  // Define initial state of the reducer/slice
  initialState: {
    user:{},
    submitted_user:{},
  },
  // Define the reducers 
  reducers: {
    update_user_action: (state, action) => {
        state.user = action.payload.user
    },
    submit_user_action: (state, action) => {
        console.log("User to Submit");
        console.log(action.payload.user);
        state.submitted_user = action.payload.user
    },
}
})

// Action creators are generated for each case reducer function
export const { update_user_action,submit_user_action } = userSlice.actions

export default userSlice.reducer
```

The 2 defined actions `update_user_action`, `submit_user_action`  would be in charge respectively of triggering current user update and current user submission.

The Reducer will be in charge of processing triggered information and updating the state of the system

- Code explanation

```javascript
...
    initialState: {
        user:{},
        submitted_user:{},
    },
  ...
```
Reducers come with a predefined system state here the **userReducer** state is composed of an object ```user:{}``` that refers to the current user value and ```submitted_user:{}``` that refers to the user we want to submit.


```javascript
...
  reducers: {
    update_user_action: (state, action) => {
        ...
    },
    submit_user_action: (state, action) => {
        ...
    },
...
```
The reducers defined here process the information. A component update would be triggered only if the previous state `state` of the reducer changes.

```javascript
...
        state.submitted_user = action.payload.user
...
```

In the `submit_user_action` reducer, the state is altered and received information coming from parameters. Note that the data hold in the parameter `action` is available in the `payload` of this parameter :  `action.payload.user`


# Store creation
In the folder **src/** create a file **store.js**:
```javascript
    import { configureStore } from '@reduxjs/toolkit';
    import userReducer from './slices/userSlice';

    export default configureStore({
        reducer: {
            userReducer: userReducer
          },
})
```
This file link the reducer to the store which is in charge of receiving data state update and providing updates to other components.
# Update main.jsx
Actions and reducers are defined. We need now to link the **store** to other components.

Update the following inside **main.jsx**
```javascript
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import {App} from './App';
import store from './store.js';

import 'semantic-ui-css/semantic.min.css'

  ReactDOM.createRoot(document.getElementById('root')).render(
      <Provider store={store} >
        <App />
      </Provider>
  )
```
- ```Provider``` is a redux tools allowing the creation of a store.

```javascript
...
  <Provider store={store} >
        <App />
    </Provider>
...
```
All components belonging to the `<Provider>` could use the created store

# Update ReactJS component to use redux store

## User component
Update the **src/components/user/containers/User.jsx** file:

```javascript
...
import { useSelector } from 'react-redux';
...
```

Add service to connect current component to the store


```javascript
...
    export const User=(props)=> {
    let current_user = useSelector(state => state.userReducer.user);
```

- ```useSelector(state => state.userReducer.user)``` allows catching state updated by reducers. As soon a reducer (here `userReducer`) return a modified state, the current component is called and the modified state value  `state.userReducer.user` is saved into `let current_user`


## UserForm component
Update the **src/components/userform/UserForm.jsx** file:

```javascript
...
import {  useDispatch } from 'react-redux';
import {update_user_action,submit_user_action } from '../../slices/userSlice';
...
```

Add service to connect current component to the store, and action to send to the store

```javascript
...
  export const UserForm = (props) =>{
     ...
    const dispatch = useDispatch();

```

Connect the current reactJS component (UserForm) to the store. When current component needs to send an action to the store, the created `dispatch` object should be used.


```javascript
...
function processInput(event, { valueData }){
        const target = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        console.log(event.target.value);
        let currentVal=currentUser;
        setCurrentUser({...currentUser, [name]: value});
        currentVal[name]= value;
        //props.handleChange(currentVal);
        dispatch(update_user_action({user:currentVal}));
    };

    function submitOrder(data){
       // props.submitUserHandler(data);
      dispatch(submit_user_action({user:currentUser}));
    }
...
```



- ``` dispatch(update_user_action({user:currentVal}));``` this line triggers an event to the store. The action   ```update_user_action``` with the parameter ```user:currentVal``` is sent to the store through the ```dispatch``` function.
- The reducers of the current store will then process this action
