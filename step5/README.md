# Use React Router

In this section, we will show basic usages of react-router version 6.

<img src="./img/display.jpg" alt="display step 4" width="600"/>

## 1. Getting start
Before starting, the react router npm package needs to be installed

```
npm install react-router-dom
```

## 2. Catching URL

- Modify the `App.jsx` file as follow:

    ```javascript
    import { BrowserRouter } from "react-router-dom";
    ...
    export const App =(props) =>{
        return (
        <>
          <BrowserRouter>
           ...
        </BrowserRouter>

        </>
      );
    }

    ```
- Code explanation
  - `import { BrowserRouter} from "react-router-dom";`: import resources for react router
  - ` <BrowserRouter> ... </BrowserRouter>`: all components that interact with web browser URL would be catched by react router.

- Update again the the `App.jsx` file as follow:

    ```javascript
    import { BrowserRouter, Routes, Route } from "react-router-dom";
    ...
    export const App =(props) =>{
        return (
        <>
            <BrowserRouter>
                <Menu>
                    <Menu.Item name='heropres'>
                            Hero Presentation
                    </Menu.Item>
                </Menu>
                <div>
                    <Routes>
                        <Route path='/display' element={<Display/>} />
                        <Route path='/form' element={<FormDisplay/>} />
                        <Route path='/' element={<Home/>} />
                    </Routes>
                </div>
            </BrowserRouter>
        </>
      );
    }

    ```
- Code explanation
  - we currently remove components that are directly called by the Main component
  - `<Routes>...<Routes>` : define a space where a component would be called depending the URL asked
  - `<Route path='/display' element={<Display/>} />` : define a conditional display, if the URL called is `/display` then the component `<Display/>` is rendered

- In the `src` folder of your project, create the folder `pages`.
- The folder contains component that will be displayed according th URL asked.

- In the `pages` folder, create the file `Home.jsx` as follow:

    ```javascript
    import React, { useState } from 'react';
    import { Container } from 'semantic-ui-react'

    export const Home =(props) =>{
      return (
        <Container>
          <h1>HOME</h1>
          </Container>
      );
    }
    ```

- In the `pages` folder create the file `Display.jsx` as follow:

    ```javascript
    import React, { useState } from 'react';
    import {useSelector } from "react-redux";
    import {User } from '../components/user/containers/User'
    import { Container } from 'semantic-ui-react'

    export const Display =(props) =>{
      let current_user = useSelector(state => state.userReducer.user);

      return (
        <Container>
          <User 
                id={current_user .id}
                surname={current_user .surname}
                lastname={current_user .lastname}
                login={current_user .login}
                pwd={current_user .pwd}
                money={current_user .money}
                img={current_user .img}
                display_type='FULL'>      
          </User>
        </Container>
      );
    }
    ```
- In the `pages` folder create the file `FormDisplay.jsx` as follow:
    ```javascript
    import React, { useState } from 'react';
    import { Grid, Segment,Container } from 'semantic-ui-react'
    import {User } from '../components/user/containers/User'
    import {UserForm } from '../components/userform/UserForm'

    export const FormDisplay =(props) =>{
    const [currentUser,setCurrentUser]= useState({
                                      id:12,
                                      surname:"John",
                                      lastname:"Doe",
                                      login:"jDoe",
                                      pwd:"jdoepwd",
                                      img:'https://www.nicepng.com/png/full/982-9820051_heart-2352306885-deadpool-png.png',
                                      money:1000,
                                    });
  
    function callbackErr(data){
      console.log(data);
    };

    function handleChange(data){
      console.log(data);
      setCurrentUser({
        id:data.id,
        surname:data.surname,
        lastname:data.lastname,
        login:data.login,
        pwd:data.pwd,
        money:data.money,
        img:data.img,
      });
    };

    function redirectHandler(data){
      console.log("user to submit"+data);
    };

    return (
      <Container>
        <Grid divided='vertically'>
          <Grid.Row columns={3}>
            <Grid.Column>
              <Segment>
                <UserForm 
                      handleChange={handleChange}
                      redirect={redirectHandler}>
                </UserForm>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <User 
                     id={currentUser.id}
                      surname={currentUser.surname}
                      lastname={currentUser.lastname}
                      login={currentUser.login}
                      pwd={currentUser.pwd}
                      money={currentUser.money}
                      img={currentUser.img}
                      display_type='FULL'>
              </User>
            </Grid.Column>
            <Grid.Column>
              <User id={currentUser.id}
                      surname={currentUser.surname}
                      lastname={currentUser.lastname}
                      login={currentUser.login}
                      pwd={currentUser.pwd}
                      money={currentUser.money}
                      img={currentUser.img}
                      display_type='SHORT'>
              </User>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
    }
    ```
- You can check that routes work by calling the URL `/`, `/form`, `\display` 
- `CAUTION` by directly calling  URL, the page will be reloaded and context erased.

## 3. Using Navigation Menu
- Modify the `App.jsx` file as follow:
    ```javascript
    ...
    import { 
      BrowserRouter,
      Routes,
      Route,
      NavLink
    } from "react-router-dom";
    ...
    export const App =(props) =>{
      const [currentUser,setCurrentUser]= useState({
                                          id:12,
                                          surname:"John",
                                          lastname:"Doe",
                                          login:"jDoe",
                                          pwd:"jdoepwd",
                                          img:'https://www.nicepng.com/png/full/982-9820051_heart-2352306885-deadpool-png.png',
                                          money:1000,
                                        });

      return (
        <Provider store={store} >
          <BrowserRouter>
          <Menu>
            <Menu.Item
              name='heropres'

            >
            <NavLink to="/" >Home</NavLink>
            </Menu.Item> 
            <Menu.Item
              name='heropres'
            >
            <NavLink to="/display">Display</NavLink>
            </Menu.Item> 
            <Menu.Item
              name='heropres'
            >
            <NavLink to="/form"> Form</NavLink>
            </Menu.Item> 
          </Menu>
          <div>

            {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
            <Routes>
            <Route path='/display' element={<Display/>} />
            <Route path='/form' element={<FormDisplay/>} />
            <Route path='/' element={<Home/>} />
            </Routes>
          </div>
        </BrowserRouter>

        </Provider>
      );
    }

    ```
- Code explanation 
  - `<NavLink to="/display">Display</NavLink>` : `NavLink` components are special react router complonents that can navigate into different pages without recharging the Web Page's context (as do `<a>` ). Clicking on such link will call react router that decides which component to display according to the `<Router>` and `<Route>` information.

- Try to navigate between pages using created links.


## 4. Navigate through javascript

In some cases, we need to call navigation through javascript. To do so, we use a tools of react router called `useNavigate`.

- Update the `FormDisplay.jsx` file as follow:
    ```javascript
    ...
    import { 
       useNavigate
    } from "react-router-dom";
    ...
    //Create function component
    export const FormDisplay =(props) =>{
      const [currentUser,setCurrentUser]= useState({
                                            ...
                                        });
      const navigate = useNavigate();

       function callbackErr(data){
          console.log(data);
      };
      ...

     function redirectHandler(data){
        console.log("user to submit"+data);

        navigate('/display');

      };
      ...
    ```
  
- Code explanation 
  -   `const navigate = useNavigate();` create a tool instance to ask for navigation.
  -   `navigate('/display');` ask react router to navigate to `/display`.


- Try the navigation by clicking on the submit button of the form. Page content change to the display page content.




## 5. Refs
- [react-router official docs](https://reactrouter.com/docs/en/v6/getting-started/concepts)
  